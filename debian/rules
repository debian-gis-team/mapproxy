#!/usr/bin/make -f

#export DH_VERBOSE=1

export PYBUILD_NAME=mapproxy
export PYBUILD_BEFORE_TEST=mkdir -p {build_dir}/mapproxy && cp -r {dir}/mapproxy/test {build_dir}/mapproxy
export PYBUILD_INSTALL_ARGS=--install-scripts=/usr/lib/{package}

include /usr/share/dpkg/pkg-info.mk

BUILD_DATE=$(shell LC_ALL=C date -u "+%d %B %Y" -d "@$(SOURCE_DATE_EPOCH)")

MANPAGES := $(wildcard debian/man/*.*.xml)

%:
	dh $@ --buildsystem=pybuild

execute_before_dh_auto_build:
	# Create man page from DocBook XML
	for x in $(MANPAGES) ; do \
	    docbook2x-man --string-param header-3="$(BUILD_DATE)" $$x ; \
	    mv `basename $$x | sed 's/.xml$$//'` `dirname $$x` ; \
	done

override_dh_auto_test:
	dh_auto_test || echo "Ignoring test failures"

execute_after_dh_auto_install:
	# Remove duplicate license file
	$(RM) debian/*/usr/lib/python*/dist-packages/mapproxy/image/fonts/LICENSE

	for V in `py3versions -v -s`; do \
		for F in DejaVuSans.ttf DejaVuSansMono.ttf; do \
			$(RM) debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/image/fonts/$$F ; \
			ln -s /usr/share/fonts/truetype/dejavu/$$F \
			      debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/image/fonts/$$F ; \
		done ; \
		if [ ! -e debian/python3-$(PYBUILD_NAME)/usr/share/python3-$(PYBUILD_NAME)/service/templates ]; then \
			mkdir -p debian/python3-$(PYBUILD_NAME)/usr/share/python3-$(PYBUILD_NAME)/service ; \
			mv debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/service/templates \
			   debian/python3-$(PYBUILD_NAME)/usr/share/python3-$(PYBUILD_NAME)/service/ ; \
		else \
			$(RM) -r debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/service/templates ; \
		fi ; \
		ln -s /usr/share/python3-$(PYBUILD_NAME)/service/templates \
		      debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/service/templates ; \
		if [ ! -e debian/python3-$(PYBUILD_NAME)/usr/share/python3-$(PYBUILD_NAME)/test/schemas ]; then \
			mkdir -p debian/python3-$(PYBUILD_NAME)/usr/share/python3-$(PYBUILD_NAME)/test ; \
			mv debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/test/schemas \
			   debian/python3-$(PYBUILD_NAME)/usr/share/python3-$(PYBUILD_NAME)/test/ ; \
		else \
			$(RM) -r debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/test/schemas ; \
		fi ; \
		ln -s /usr/share/python3-$(PYBUILD_NAME)/test/schemas \
		      debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/test/schemas ; \
		if [ ! -e debian/python3-$(PYBUILD_NAME)/usr/share/python3-$(PYBUILD_NAME)/test/system/fixture/cache_data ]; then \
			mkdir -p debian/python3-$(PYBUILD_NAME)/usr/share/python3-$(PYBUILD_NAME)/test/system/fixture ; \
			mv debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/test/system/fixture/cache_data \
			   debian/python3-$(PYBUILD_NAME)/usr/share/python3-$(PYBUILD_NAME)/test/system/fixture/ ; \
		else \
			$(RM) -r debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/test/system/fixture/cache_data ; \
		fi ; \
		ln -s /usr/share/python3-$(PYBUILD_NAME)/test/system/fixture/cache_data \
		      debian/python3-$(PYBUILD_NAME)/usr/lib/python$$V/dist-packages/mapproxy/test/system/fixture/cache_data ; \
	done

execute_after_dh_install:
	$(RM) debian/*/usr/share/python*-mapproxy/test/schemas/*/*/ReadMe.txt
