Source: mapproxy
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Bas Couwenberg <sebastic@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-python3,
               libgdal-dev,
               python3-all,
               python3-jsonschema,
               python3-lxml,
               python3-pil,
               python3-pyproj,
               python3-pytest,
               python3-redis,
               python3-requests,
               python3-setuptools,
               python3-shapely,
               python3-webtest,
               python3-werkzeug,
               python3-yaml,
               docbook2x,
               docbook-xsl,
               docbook-xml,
               xsltproc
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/mapproxy
Vcs-Git: https://salsa.debian.org/debian-gis-team/mapproxy.git
Homepage: http://mapproxy.org/
Rules-Requires-Root: no

Package: mapproxy
Architecture: all
Section: web
Depends: python3-mapproxy (= ${binary:Version}),
         ${misc:Depends}
Description: open source proxy for geospatial data
 MapProxy is an open source proxy for geospatial data. It caches, accelerates
 and transforms data from existing map services and serves any desktop or web
 GIS client.
 .
 MapProxy is a tile server (WMS-C, TMS, WMTS, KML SuperOverlays), and also a
 fully compliant WMS server supporting any WMS client (desktop and web).
 .
 This package provides the mapproxy utilities.

Package: python3-mapproxy
Architecture: all
Depends: fonts-dejavu-core,
         ${python3:Depends},
         ${misc:Depends}
Recommends: python3-azure-storage,
            python3-boto3,
            python3-botocore,
            python3-gdal,
            python3-lxml,
            python3-pastedeploy,
            python3-redis,
            python3-shapely
Description: open source proxy for geospatial data - Python 3 module
 MapProxy is an open source proxy for geospatial data. It caches, accelerates
 and transforms data from existing map services and serves any desktop or web
 GIS client.
 .
 MapProxy is a tile server (WMS-C, TMS, WMTS, KML SuperOverlays), and also a
 fully compliant WMS server supporting any WMS client (desktop and web).
 .
 This package provides the mapproxy module for Python 3.
